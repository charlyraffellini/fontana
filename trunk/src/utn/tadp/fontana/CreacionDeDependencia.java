package utn.tadp.fontana;

public interface CreacionDeDependencia {

	Object getObject();

	CreacionDeDependencia setClass(Class cClass);

}
